set(errors_SRCS
	${CMAKE_CURRENT_SOURCE_DIR}/errorfinder.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/errortracker.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/finderrorsdialog.cpp
	CACHE INTERNAL EXPORTEDVARIABLE
)
