set(gui_SRCS
	${CMAKE_CURRENT_SOURCE_DIR}/currentlinewidget.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/linesitemdelegate.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/linesmodel.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/linesselectionmodel.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/lineswidget.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/playerwidget.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/waveformwidget.cpp
	CACHE INTERNAL EXPORTEDVARIABLE
)
